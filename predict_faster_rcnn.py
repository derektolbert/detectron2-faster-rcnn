import detectron2

# import some common libraries
import cv2, pdb, os, random
import matplotlib.pyplot as plt

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog

CLASSES = [
    "abstract", "author", "caption", "date", "equation", "figure", "footer", 
    "list", "paragraph", "reference", "section", "table", "title",
]

BASE_DIR = "/home/aptus/Clients/Pinetree/Stax/stax-modules/data/DocBank_500K_ori_img/"
TESTS = [ BASE_DIR + f for f in os.listdir(BASE_DIR) ]

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
cfg.MODEL.WEIGHTS = "model_final.pth"
# cfg.MODEL.DEVICE = "cpu"
cfg.DATASETS.TRAIN = ("signature_train",)
cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.00025 
cfg.SOLVER.MAX_ITER = 300
cfg.SOLVER.STEPS = []
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128
cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(CLASSES)

import time

# Create predictor
predictor = DefaultPredictor(cfg)

for filepath in random.sample(TESTS, 10):

    start = time.time()

    # Predict image
    im = cv2.imread(filepath)
    outputs = predictor(im)
    v = Visualizer(
        im, 
        metadata=MetadataCatalog.get("test").set(thing_classes=CLASSES),
        scale=0.5
    )
    out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    print(f"That took {time.time() - start}s to predict")

    if len(outputs["instances"]) > 0:

        plt.imshow(out.get_image())
        plt.show()

    else:
        print(f"predicted {filepath}")
    