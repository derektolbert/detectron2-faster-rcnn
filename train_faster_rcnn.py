import detectron2

# import some common libraries
import cv2, pdb, os, random
import matplotlib.pyplot as plt

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor, DefaultTrainer
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.structures import BoxMode

# Data dir
LABELS = "/home/aptus/Clients/Pinetree/Stax/signature_detection/tobacco_800_labels.txt"

def get_dataset(labelPath: str) -> list:

    # Map filename -> [bbox_coords, label]
    labels: dict = {}
    class_map: dict = {}
    with open(labelPath, "r") as f:
        labelStr = f.read()
        
        # Parse label string
        def parseLine(line: str):
            splitLine = line.split(",")
            curr_data = labels.get(splitLine[0], [])

            if len(splitLine[0]) > 0:

                # Get unique int id for each class label
                if splitLine[-1] in class_map:
                    splitLine[-1] = class_map[splitLine[-1]]
                else:
                    class_map[splitLine[-1]] = len(class_map.keys())
                    splitLine[-1] = class_map[splitLine[-1]]

                curr_data.append([int(val) for val in splitLine[1:] ])
                labels[splitLine[0]] = curr_data

        [ parseLine(l) for l in labelStr.split("\n") ]

    dataset: list = []
    count = 0
    for filename, data in labels.items():
        record: dict = {}

        # Get dims
        h, w = cv2.imread(filename).shape[:2]

        # Update record meta
        record["file_name"] = filename
        record["image_id"] = count
        record["height"] = h
        record["width"] = w

        # Parse bounding box annotations
        annotations: list = []
        for box in data:
            annotations.append({
                "bbox": box[:4],
                "bbox_mode": BoxMode.XYXY_ABS,
                "category_id": box[-1]
            })

        record["annotations"] = annotations
        dataset.append(record)
        count += 1

    return dataset

# Load dataset
DatasetCatalog.register("signature_train", lambda : get_dataset(LABELS))
MetadataCatalog.get("signature_train").set(thing_classes=["signature"])
metadata = MetadataCatalog.get("signature_train")

cfg = get_cfg()

# add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml")
cfg.MODEL.DEVICE = "cpu"
cfg.DATASETS.TRAIN = ("signature_train",)
cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.00025 
cfg.SOLVER.MAX_ITER = 300
cfg.SOLVER.STEPS = []
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1 

os.makedirs("output", exist_ok=True)
trainer = DefaultTrainer(cfg)
trainer.resume_or_load(resume=False)
trainer.train()